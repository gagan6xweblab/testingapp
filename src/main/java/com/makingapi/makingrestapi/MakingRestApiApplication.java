package com.makingapi.makingrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MakingRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MakingRestApiApplication.class, args);
	}

}
